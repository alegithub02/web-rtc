# PROYECTO Articulo Introduccion a Web RTC"
## INSTALACIONES PREVIAS

*Crear cuenta en Agora.io*

Dirijase al siguiente link:

https://www.agora.io/en/

Cree una cuenta y un Nuevo Proyecto, es necesario copiar la AppId de su Nuevo Proyecto
y pegarlo en la primera linea del archivo main.js.

## let APP_ID = "Crear una app-id en Agora.io y pegar AQUI"

Esto establecera una primera comunicacion, de otra forma no funcionara el proyecto.


## FUNCIONAMIENTO

Simplemente abra el archivo lobby.html o index.html con Open Live Server, ingresara al lobby de la aplicacion, ingrese un
numero de sala, puede ser cualquier numero. Para establecer una videollamada otro usuario debe abrir uno de los dos archivos
anteriores y una vez en el lobby debe ingresar el mismo numero.

Ya dentro del lobby debemos dar permisos de camara y microfono. Ahi mismo tenemos los botones para encender/apagar video, encender/apagar microfono y salir de la sala de reunion. 

# Autores

- Jorge Alejandro Garcia
- José Alejandro Cáceres Corrales

La aplicacion esta basasda en el trabajo de Dennis Ivy cuyo repositorio de GitHub es 
https://github.com/divanov11/PeerChat